from sqlalchemy import Column, ForeignKey, Integer, String, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from . import engine


Base = declarative_base()


product_substance_maps = Table('npl_product_substance_maps', Base.metadata,
    Column('product_id', Integer, ForeignKey('npl_products.id')),
    Column('substance_id', Integer, ForeignKey('npl_substances.id')),
)


class Product(Base):
    __tablename__ = 'npl_products'

    id = Column(Integer, primary_key=True)
    foreign_id = Column(String)
    name = Column(String)
    substances = relationship(
        'Substance', secondary=product_substance_maps, backref='products')

    def __init__(self, foreign_id, name):
        self.foreign_id = foreign_id
        self.name = name

    def __repr__(self):
        return "<Product('{0.foreign_id}', '{0.name}')>".format(self)


class Substance(Base):
    __tablename__ = 'npl_substances'

    id = Column(Integer, primary_key=True)
    foreign_id = Column(String)
    name = Column(String)

    def __init__(self, foreign_id, name):
        self.foreign_id = foreign_id
        self.name = name

    def __repr__(self):
        return "<Substance('{0.foreign_id}', '{0.name}')>".format(self)


Base.metadata.create_all(engine)
