from os import makedirs
from os.path import abspath, dirname, exists, join

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


DB_DIRECTORY = join(dirname(abspath(__file__)), '..', '..', 'db')
DB_FILE = 'npl.db'
DB_PATH = join(DB_DIRECTORY, DB_FILE)
if not exists(DB_DIRECTORY):
    makedirs(DB_DIRECTORY)

engine = create_engine('sqlite:///{}'.format(DB_PATH))
Session = sessionmaker(bind=engine)
