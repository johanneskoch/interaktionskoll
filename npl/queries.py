from . import Session
from .exceptions import ProductsNotFound
from .models import Product, Substance


def find_products(*product_names):
    session = Session()

    products = session.query(
        Product.name
    ).filter(
        Product.name.in_(product_names)
    ).group_by(
        Product.name
    ).all()
    return [product[0] for product in products]


def substances_from_products(*products):
    session = Session()

    products_not_found = set(products) - set(find_products(*products))
    if products_not_found:
        raise ProductsNotFound(products_not_found)

    substances = session.query(
        Substance
    ).join(
        Substance.products
    ).filter(
        Product.name.in_(products)
    ).all()
    return [substance.name for substance in substances]
