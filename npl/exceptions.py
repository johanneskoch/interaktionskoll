class ParseError(Exception):
	pass


class ProductsNotFound(Exception):
    def __init__(self, products):
        message = ', '.join(products)
        Exception.__init__(self, message)
