import sys
from collections import defaultdict

from lxml import etree
from sqlalchemy import sql

from . import Session
from .exceptions import ParseError
from .models import Product, Substance, product_substance_maps


SUBSTANCE_PATH = '{urn:schemas-npl:instance}substance'
PRODUCT_PATH = '{urn:schemas-npl:instance}medprod'
SUBSTANCE_NAME_PATH = '{urn:schemas-npl:industry}name'
PRODUCT_ID_PATH = '/'.join([
    '{urn:schemas-npl:instance}identifiers',
    '{urn:schemas-npl:mpa}identifier[@type="nplid"]',
])
PRODUCT_NAME_PATH = '/'.join([
    '{urn:schemas-npl:instance}names',
    '{urn:schemas-npl:industry}name[@type="tradename"]',
])
PRODUCT_SUBSTANCES_PATH = '/'.join([
    '{urn:schemas-npl:instance}pharmaceutical-products',
    '{urn:schemas-npl:instance}pharmaceutical-product',
    '{urn:schemas-npl:instance}composition',
    '{urn:schemas-npl:instance}ingredient',
    '{urn:schemas-npl:instance}substance-lx',
])


def parse_substance(substance):
    substance_id = substance.get('id')
    substance_name = substance.find(SUBSTANCE_NAME_PATH).get('v')

    return Substance(substance_id, substance_name)


def parse_product(product):
    product_id = product.find(PRODUCT_ID_PATH).get('v')
    product_name = product.find(PRODUCT_NAME_PATH).get('v')

    # Remove registered trademark symbol from name, SFINX doesn't use those
    product_name = product_name.replace(u'\u00AE', '')

    return Product(product_id, product_name)


def parse_product_substances(product, session):
    substances = product.findall(PRODUCT_SUBSTANCES_PATH)
    substance_ids = [substance.get('v') for substance in substances]

    # Some ingredients have empty <substance-lx> elements
    substance_ids = filter(None, substance_ids)

    # Sometimes, substances are listed more than once
    substance_ids = set(substance_ids)

    # Some <pharmaceutical-products> or <composition> elements are empty
    if len(substance_ids):
        substance_objs = session.query(Substance).filter(
            Substance.foreign_id.in_(substance_ids)).all()
    else:
        substance_objs = session.query(Substance).filter(sql.false()).all()

    # Unapproved or withdrawn products may contain uncatalogued substances
    if len(substance_objs) != len(substance_ids):
        raise ParseError

    return substance_objs


def update(substances_filepath, products_filepath):
    session = Session()
    counter = defaultdict(int)

    for _, element in etree.iterparse(substances_filepath, tag=SUBSTANCE_PATH):
        substance = parse_substance(element)
        element.clear()
        session.add(substance)
        counter['substances'] += 1

    for _, element in etree.iterparse(products_filepath, tag=PRODUCT_PATH):
        product = parse_product(element)

        try:
            product.substances = parse_product_substances(element, session)
        except ParseError:
            continue
        finally:
            element.clear()

        session.add(product)
        counter['products'] += 1
        counter['maps'] += len(product.substances)

    session.commit()

    assert(counter['substances'] == session.query(Substance).count())
    assert(counter['products'] == session.query(Product).count())
    assert(counter['maps'] == session.query(product_substance_maps).count())

    print 'Added', ', '.join(['{1} {0}'.format(*x) for x in counter.items()])


if __name__ == '__main__':
    update(*sys.argv[1:])
