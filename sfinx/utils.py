import subprocess
import tempfile

from .constants import (
    MODE_BRANDNAME, VIEW_INDEX, VIEW_SEARCH, VIEW_INTERACTIONS,
    VIEW_ALL_INTERACTIONS, VIEW_DETAILS, REMOVE_NONE, PRINT_HOSTILE,
)


API_DATA = {
    'searchStr': '',
    'mode': MODE_BRANDNAME,
    'view': VIEW_INDEX,
    'iNew': '',
    'remove': REMOVE_NONE,
    'langCode': 'sv',
    'optdrug': '',
    'optjanus': 1,
    'optprint': PRINT_HOSTILE,
}


def search_data(mode, *drugs):
    data = API_DATA.copy()
    data.update(mode=mode, view=VIEW_SEARCH, searchStr=drugs[-1])
    for index, drug in enumerate(drugs[:-1]):
        data['i{}'.format(index)] = drug
    return data


def interactions_data(mode, *drugs):
    data = API_DATA.copy()
    data.update(mode=mode, view=VIEW_INTERACTIONS)
    for index, drug in enumerate(drugs):
        data['i{}'.format(index)] = drug
    return data


def all_interactions_data(mode, drug):
    data = API_DATA.copy()
    data.update(mode=mode, view=VIEW_ALL_INTERACTIONS, searchStr=drug)
    return data


def details_data(interaction_id):
    data = dict(view=VIEW_DETAILS, interactionId=interaction_id)
    return data


def show(response):
    tmp = tempfile.NamedTemporaryFile(delete=False, suffix='.html')
    tmp.write(str(response.encode('utf-8')))
    subprocess.call(['open', '-a', 'Google Chrome', tmp.name])
