from wtforms import Form, IntegerField, StringField
from wtforms.compat import iteritems

from .constants import PROFESSION_EMPTY, PROFESSIONS


class RenamedFieldsForm(Form):
    @property
    def renamed_data(self):
        return {f.id: f.data for name, f in iteritems(self._fields)}


class ProfessionField(IntegerField):
    def process_formdata(self, valuelist):
        if valuelist and valuelist[0] in PROFESSIONS:
            value = PROFESSIONS[valuelist[0]]
        else:
            value = PROFESSION_EMPTY
        return super(ProfessionField, self).process_formdata([value])


class LoginForm(RenamedFieldsForm):
    email = StringField()
    password = StringField()
    reqPage = StringField()


class SignupForm(RenamedFieldsForm):
    email = StringField()
    password = StringField()
    first_name = StringField(id='firstName')
    last_name = StringField(id='surname')
    profession = ProfessionField()
    city = StringField()
    reqPage = StringField()


class ActivationForm(RenamedFieldsForm):
    code = StringField(id='activationCode')
    reqPage = StringField()
