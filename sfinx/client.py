from urlparse import parse_qs, urlparse

import requests

from .constants import (
    VIEW_INDEX, VIEW_DISAMBIGUATION, VIEW_SEARCH, VIEW_INTERACTIONS,
    VIEW_ALL_INTERACTIONS, VIEW_DETAILS, MODES, INVERSE_MODES,
)
from .exceptions import (
    NotAuthenticated, InvalidMode, UnexpectedResponse, ServerError)
from forms import LoginForm, SignupForm, ActivationForm
from .parsers import parse, parse_form_errors
from .utils import (
    search_data, interactions_data, all_interactions_data, details_data)


HTTP_PROXY = ':'
LOGIN_URL = 'http://www.janusinfo.se/sfinx/security/login.html'
SIGNUP_URL = 'http://www.janusinfo.se/sfinx/security/register.html'
ACTIVATION_URL = 'http://www.janusinfo.se/sfinx/security/activation.html'
API_URL = 'http://www.janusinfo.se/sfinx/interactions/index.jsp'
ERROR_URL = 'http://www.janusinfo.se/sfinx/interactions/serverError.jsp'


class JanusClient(object):
    def __init__(self, session_id=None):
        self.session_id = session_id
        self.session = requests.Session()
        # self.session.proxies = {'http': HTTP_PROXY}
        if self.session_id is not None:
            self.session.cookies['JSESSIONID'] = session_id

        self._mode = None

    @property
    def mode(self):
        return INVERSE_MODES.get(self._mode)

    @mode.setter
    def mode(self, value):
        try:
            self._mode = None if value is None else MODES[value]
        except KeyError:
            raise InvalidMode(value)

    def api_request(self, expected_views, **kwargs):
        method = 'POST' if 'data' in kwargs else 'GET'
        response = self.session.request(method, API_URL, **kwargs)

        if response.status_code != requests.codes.ok:
            raise UnexpectedResponse(response.status_code)
        if response.url.startswith(ERROR_URL):
            raise ServerError(
                parse_qs(urlparse(response.url).query).get('message', [''])[0])
        if response.url.startswith(LOGIN_URL):
            raise NotAuthenticated

        return parse(response.text, expected_views, True)

    def login(self, **form_data):
        form = LoginForm(**form_data)
        self.session.post(LOGIN_URL, data=form.renamed_data)

        try:
            self.session_id = self.session.cookies['JSESSIONID']
        except KeyError:
            raise NotAuthenticated

    def signup(self, **form_data):
        form = SignupForm(**form_data)
        form.validate()
        response = self.session.post(SIGNUP_URL, data=form.renamed_data)

        if response.url.startswith(ACTIVATION_URL):
            return True, {}
        else:
            return False, parse_form_errors(response.text)

    def activate(self, **form_data):
        form = ActivationForm(**form_data)
        form.validate()
        response = self.session.post(ACTIVATION_URL, data=form.renamed_data)

        if response.url.startswith(LOGIN_URL):
            return True, {}
        else:
            return False, parse_form_errors(response.text)

    def ping(self):
        return self.api_request((VIEW_INDEX,))

    def _get_search(self, mode, query):
        search_results = self.api_request(
            (VIEW_DISAMBIGUATION, VIEW_SEARCH), data=search_data(mode, query))

        if search_results is None:
            return []
        elif isinstance(search_results, dict):
            return [search_results]
        return search_results

    def get_search(self, query):
        search_results = []
        for mode in MODES.values() if self._mode is None else [self._mode]:
            search_results.extend(self._get_search(mode, query))
        return search_results

    def get_interactions(self, *drugs):
        return self.api_request(
            (VIEW_INTERACTIONS,), data=interactions_data(self._mode, *drugs))

    def get_all_interactions(self, drug):
        try:
            return self.api_request(
                (VIEW_ALL_INTERACTIONS,),
                data=all_interactions_data(self._mode, drug),
            )
        except ServerError as e:
            if e.message == 'Fel i getAllInteractions: null':
                # Invalid drugs and interactionless ones both return this error
                search_results = self.get_search(drug)
                if drug in [result['name'] for result in search_results]:
                    return []
            raise

    def get_details(self, interaction_id):
        return self.api_request(
            (VIEW_DETAILS,), params=details_data(interaction_id))
