from .constants import MODES


class JanusError(Exception):
    pass


class NotAuthenticated(JanusError):
    pass


class InvalidMode(JanusError):
    def __init__(self, mode):
        message = 'Invalid mode: {mode}; must be {expected} or None'.format(
            mode=mode, expected=', '.join(MODES))
        Exception.__init__(self, message)


class UnexpectedResponse(JanusError):
    def __init__(self, status_code):
        message = 'Unexpected response status: {}'.format(status_code)
        Exception.__init__(self, message)


class UnexpectedView(JanusError):
    def __init__(self, view, expected_views):
        message = 'Received view {view} but expected one of {expected}'.format(
            view=view, expected=', '.join(expected_views))
        Exception.__init__(self, message)


class ServerError(JanusError):
    def __init__(self, error_message):
        message = error_message or 'Received error response from Janusinfo'
        Exception.__init__(self, message)
