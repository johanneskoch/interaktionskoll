import re
from urlparse import parse_qs, urlparse

from bs4 import BeautifulSoup

from .constants import (
    VIEW_INDEX, VIEW_DISAMBIGUATION, VIEW_SEARCH, VIEW_INTERACTIONS,
    VIEW_ALL_INTERACTIONS, VIEW_DETAILS, INVERSE_MODES,
)
from .exceptions import UnexpectedView


def parse_mode(soup):
    # Could be refactored into a function that parses the entire set of options
    mode = int(soup.form.find(attrs={'name': 'mode'})['value'])
    return INVERSE_MODES.get(mode)


def parse_drug_from_disambiguation(drug):
    return {'name': drug.string}


def parse_substance_from_interaction(substance):
    return {
        'name': substance.stripped_strings.next().rstrip(','),
        'route_of_administration': substance.a.string,
    }


def parse_interaction(interaction):
    classification = interaction.find(class_=re.compile('^sfinxDdiCls_[A-D]$'))
    text = interaction.find(class_=re.compile('^sfinxDdiTextBx_[A-D]$'))
    substances = text.find_all('h2')[:-2]
    information = text.find_all('p', class_='section')
    actions = text.find(class_='alignRight')
    return {
        'id': parse_qs(urlparse(actions.a['href']).query)['interactionId'][0],
        'classification': classification.a.string,
        'substances': map(parse_substance_from_interaction, substances),
        'consequence': information[0].string,
        'recommendation': information[1].string,
    }


def parse_substance_from_details(substance):
    return {
        'name': substance[0].string,
        'route_of_administration': substance[1].string,
    }


def parse_reference(reference):
    return {
        'source': reference.stripped_strings.next(),
        'link': None if not reference.a else {
            'url': reference.a['href'],
            'description': reference.a.string,
        },
    }


def parse_disambiguation(soup):
    drugs = soup.find_all('a')[1:-1]
    parsed_drugs = map(parse_drug_from_disambiguation, drugs)

    mode = parse_mode(soup)
    for drug in parsed_drugs:
        drug['type'] = mode

    return parsed_drugs


def parse_search(soup):
    drug = soup.form.find(attrs={'name': 'i0'})
    try:
        return {
            'type': parse_mode(soup),
            'name': drug['value'],
        }
    except TypeError:
        return None


def parse_interactions(soup):
    interactions = soup.find_all(class_='sfinxDdiBx')
    return map(parse_interaction, interactions)


def parse_details(soup):
    actions = soup.find(class_='alignRight')
    table = zip(*map(lambda row: row.find_all('td'), soup.find_all('tr')))
    information = soup.find_all(class_='section')
    references = soup.find_all('li')
    return {
        'id': parse_qs(urlparse(actions.a['href']).query)['interactionId'][0],
        'classification': table[0][1].string,
        'substances': map(parse_substance_from_details, table[1:]),
        'consequence': information[0].string,
        'recommendation': information[1].string,
        'mechanism': information[2].string,
        'background': information[3].string,
        'references': map(parse_reference, references),
    }


PARSERS = {
    VIEW_INDEX: lambda soup: {},
    VIEW_DISAMBIGUATION: parse_disambiguation,
    VIEW_SEARCH: parse_search,
    VIEW_INTERACTIONS: parse_interactions,
    VIEW_ALL_INTERACTIONS: parse_interactions,
    VIEW_DETAILS: parse_details,
}


def parse(html, expected_views=None, strict=False):
    if expected_views is None:
        expected_views = ()

    soup = BeautifulSoup(html)
    view = int(soup.form.find(attrs={'name': 'view'})['value'])

    if strict and view not in expected_views:
        raise UnexpectedView(view, expected_views)

    return PARSERS[view](soup)


def parse_field(field):
    name = field.find(['input', 'select'])['name']
    error = field.find(class_='error').string
    return name, error


def parse_form_errors(html):
    soup = BeautifulSoup(html)
    fields = soup.find_all('tr')
    return {name: error for name, error in map(parse_field, fields) if error}
