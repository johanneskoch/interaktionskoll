Interaktionskoll
================

*Interaktionskoll* (Swedish for Interaction-check) is a service for looking up drug interactions between brandnames and substances, primarily those available on the Swedish market.

The service uses data from the *SFINX* database, obtained by screen-scraping the *Janusinfo* website, and thus requires a login to the latter. Also note that Janusinfo is only available using a Swedish IP address.

A hosted version is available at [https://interaktionskoll.se/]

The source for the web client is available in the [Interaktionskoll Client] repository.

Why not just use Janusinfo?
---------------------------

* The Janusinfo website is not optimized for mobile devices. Interaktionskoll comes with a beautifully designed interface that looks good on any screen size.
* Janusinfo allows you to look up interactions between either brandnames of substances, but not combinations of the two. By cross-referencing the *Nationella Produktregistret för Läkemedel* (NPL), Interaktionskoll allows users to make mixed queries.

Requirements
------------

* [Git] - This is a Git repository, so... yeah.
* [virtualenv] - Installing the project in a virtual environment is not strictly necessary, but highly recommended, so the installation instructions assume you have it installed.
* [curl] - Used by the script that downloads the NPL data.
* [SQLite] - Used to store the data from NPL. Since Interaktionskoll uses *SQLAlchemy*, it's easy enough to use any other database you might prefer.
* [Redis] - Used to cache responses so as to lighten the load on Janusinfo's server.
* Development packages - A development version of *Python* is needed to build some of the Python packages, as well as development versions of *libxml2* and *libxslt*.

Here's what you would need in *Debian*/*Ubuntu*:

```sh
sudo apt-get install git-core python-virtualenv curl sqlite3 redis-server python-dev libxml2-dev libxslt1-dev
```

Installation
------------

Getting the project running locally shouldn't be too much of a problem if you have some experience with web development in Python. These steps will get you started:

```sh
virtualenv interaktionskoll
cd interaktionskoll
source bin/activate
git clone https://johanneskoch@bitbucket.org/johanneskoch/interaktionskoll.git
cd interaktionskoll
pip install -r requirements/default.txt
./npl_update.sh
python runserver.py
```

Usage
-----

TBD

Troubleshooting
---------------

**Q:** The installation went fine, but when I go to http://127.0.0.1:5000/, I get a message saying "A server error occurred." What's wrong?

**A:** Did you see the part about needing a Swedish IP address? On load, the web app makes a request to the locally running server, which in turn makes a request to Janusinfo. If you're not located in Sweden, this will fail, resulting in an error message.

**Q:** Ok, that explains it. What do I do?

**A:** In the file `runserver.py`, modify the line `app.run(debug=False)` to say `app.run(debug=True)`. This will cause all requests made by the web app to be made to the hosted version of the API. This is useful if you mainly want to play around with the client, not the server. Please note that this probably violates the *same-origin policy* of your browser. How to disable this security feature depends on your browser, but can be googled easily.

**Q:** Is there a way for me to play around with the server if I don't have a Swedish IP address?

**A:** [Yes.]

References
----------

* [SFINX]
* [Janusinfo]
* [Nationella Produktregistret för Läkemedel]

[https://interaktionskoll.se/]:https://interaktionskoll.se/
[Interaktionskoll Client]:https://bitbucket.org/johanneskoch/interaktionskoll-client
[Git]:http://git-scm.com/
[virtualenv]:http://www.virtualenv.org/
[curl]:http://curl.haxx.se/
[SQLite]:https://sqlite.org/
[Redis]:http://redis.io/
[Yes.]:http://docs.python-requests.org/en/latest/user/advanced/#proxies
[SFINX]:http://www.medbase.fi/sfinx/
[Janusinfo]:http://www.janusinfo.se/Beslutsstod/Interaktioner/Interaktioner-Sfinx/
[Nationella Produktregistret för Läkemedel]:http://nsl.mpa.se/
