class Alert(dict):
    @classmethod
    def error(cls, message):
        return cls(level='danger', message=message)

    @classmethod
    def warning(cls, message):
        return cls(level='warning', message=message)

    @classmethod
    def info(cls, message):
        return cls(level='info', message=message)

    @classmethod
    def success(cls, message):
        return cls(level='success', message=message)
