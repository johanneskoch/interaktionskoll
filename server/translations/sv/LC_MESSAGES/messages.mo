��          �               �     �  
   �     �     �  (   �     �     
  	     <   "     _     v  3     5   �  -   �       
   &     1     B     T  <   \     �     �  ~  �     L     c     l     �  %   �  )   �     �     �  A        D  	   ^  8   h  /   �  9   �       
     �   %     �     �  9   �     -  )   F   Back to search Background Consequence Email address Following brandnames were not found: {}. I want to sign up as a new user Login failed. Mechanism Mixing brandnames and substances is an experimental feature. No interactions found. Password Please enter at least one other drug and try again. Please enter at least three characters and try again. Please enter two or more drugs and try again. Recommendation References SITE_DESCRIPTION Show interactions Sign in You have exceeded the maximum number of requests per minute. You need to be logged in. Your session has expired. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2014-02-10 17:57+0100
PO-Revision-Date: 2014-01-17 13:33+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: sv <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 1.3
 Tillbaka till sökning Bakgrund Medicinsk konsekvens E-postadress Följande preparat hittades inte: {}. Jag vill registrera mig som ny användare Inloggningen misslyckades. Mekanism Att blanda preparat och substanser är en experimentell funktion. Inga interaktioner funna. Lösenord Vänligen ange minst ett läkemedel till och prova igen. Vänligen ange minst tre tecken och prova igen. Vänligen ange två eller fler läkemedel och prova igen. Rekommendation Referenser <em>Interaktionskollen</em> använder data från <em>SFINX interaktionstjänst</em> som hämtas via webbplatsen <em>Janusinfo</em>. Tjänsten är gratis, men kräver inloggning. Visa interaktioner Logga in Du har överskridit det maximala antalet anrop per minut. Du måste vara inloggad. Sessionen har gått ut – logga in igen. 