from flask import g, jsonify, make_response, render_template, request
from flask.ext.babel import gettext

from . import app
from .alerts import Alert
from .decorators import api_resource
from .exceptions import ApiError
from .dehydrators import (
    dehydrate_interaction, dehydrate_interactions, dehydrate_drugs)
from npl.exceptions import ProductsNotFound
from npl.queries import substances_from_products
from sfinx.client import JanusClient
from sfinx.exceptions import NotAuthenticated


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/user')
def user():
    client = JanusClient(request.cookies.get(app.config['COOKIE_KEY']))
    try:
        client.ping()
        logged_in = True
    except NotAuthenticated:
        logged_in = False
    return jsonify(alert=None, data={'logged_in': logged_in})


@app.route('/user/login', methods=['POST'])
def login():
    client = JanusClient()
    try:
        client.login(**request.form)
    except NotAuthenticated:
        raise ApiError(gettext('Login failed.'))
    resp = make_response()
    resp.set_cookie(
        app.config['COOKIE_KEY'],
        client.session_id,
        app.config['COOKIE_MAX_AGE'],
    )
    return resp


@app.route('/user/signup', methods=['POST'])
def signup():
    client = JanusClient()
    client.signup(**request.form)
    return jsonify()


@app.route('/user/activate', methods=['POST'])
def activate():
    client = JanusClient()
    client.activate(**request.form)
    return jsonify()


@app.route('/interactions')
@api_resource(dehydrate_interactions)
def interaction_list():
    """ Example: /interactions?substance=alkohol&substance=disulfiram """
    brandnames = request.args.getlist('brandname')
    substances = request.args.getlist('substance')
    alert = None

    if brandnames and substances:
        g.client.mode, drugs = 'substance', substances

        try:
            drugs += substances_from_products(*brandnames)
        except ProductsNotFound as e:
            raise ApiError(gettext(
                'Following brandnames were not found: {}.').format(e.message))

        alert = Alert.warning(gettext(
            'Mixing brandnames and substances is an experimental feature.'))
    elif brandnames:
        g.client.mode, drugs = 'brandname', brandnames
    elif substances:
        g.client.mode, drugs = 'substance', substances
    else:
        raise ApiError(gettext(
            'Please enter two or more drugs and try again.'))

    if len(drugs) < 2:
        raise ApiError(gettext(
            'Please enter at least one other drug and try again.'))

    interactions = g.client.get_interactions(*drugs)

    if not interactions:
        alert = alert or Alert.info(gettext('No interactions found.'))

    return interactions, alert


@app.route('/interactions/<interaction_id>')
@api_resource(dehydrate_interaction)
def interaction_detail(interaction_id):
    """ Example: /interactions/11876 """
    return g.client.get_details(interaction_id)


@app.route('/search')
@api_resource(dehydrate_drugs)
def drug_search():
    """ Example: /search?q=dis&type=substance OR /search?q=totalbiscuit """
    query = request.args.get('q', '')
    if len(query) < 3:
        raise ApiError(gettext(
            'Please enter at least three characters and try again.'))

    g.client.mode = request.args.get('type')
    return g.client.get_search(query)


@app.route('/<drug_type:drug_type>/<drug_name:drug_name>/interactions')
@api_resource(dehydrate_interactions)
def drug_interactions(drug_type, drug_name):
    """ Example: /substances/alkohol/interactions """
    alert = None

    g.client.mode = drug_type
    interactions = g.client.get_all_interactions(drug_name)

    if not interactions:
        alert = Alert.info(gettext('No interactions found.'))

    return interactions, alert
