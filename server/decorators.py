from functools import wraps

from flask import abort, g, jsonify, request

from . import app, cache
from .utils import rate_limit_check
from sfinx.client import JanusClient
from sfinx.exceptions import NotAuthenticated


def api_resource(dehydrator=lambda x: x):
    def decorator(view):
        @wraps(view)
        def decorated_function(*args, **kwargs):
            rate_limit_key = u'rate-limit-{}'.format(request.remote_addr)
            resp_cache_key = u'resp-cache-{}'.format(request.full_path)

            rate_limit_check(rate_limit_key, *app.config['RATE_LIMIT'])

            if not app.config['COOKIE_KEY'] in request.cookies:
                abort(401)

            resp = cache.get(resp_cache_key)
            if resp is not None:
                return resp

            g.client = JanusClient(request.cookies[app.config['COOKIE_KEY']])

            try:
                response = view(*args, **kwargs)
            except NotAuthenticated:
                abort(401)

            if isinstance(response, tuple):
                data, alert = response
            else:
                data, alert = response, None

            resp = jsonify(alert=alert, data=dehydrator(data))
            cache.set(resp_cache_key, resp, app.config['CACHE_TIMEOUT'])
            return resp
        return decorated_function
    return decorator
