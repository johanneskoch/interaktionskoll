from flask import jsonify, request
from flask.ext.babel import gettext

from . import app
from .alerts import Alert
from .exceptions import ApiError


@app.errorhandler(401)
def unauthorized(error):
    if app.config['COOKIE_KEY'] in request.cookies:
        alert_message = gettext('Your session has expired.')
    else:
        alert_message = gettext('You need to be logged in.')
    resp = jsonify(alert=Alert.warning(alert_message), data=None)
    resp.set_cookie(app.config['COOKIE_KEY'], expires=0)
    return resp, 401


@app.errorhandler(ApiError)
def api_error(error):
    return jsonify(alert=Alert.error(error.message), data=None), 400
