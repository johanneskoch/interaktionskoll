from flask import url_for


def add_details_uri(interaction):
    interaction['details_uri'] = url_for(
        'interaction_detail', interaction_id=interaction['id'])


def add_interactions_uri(drug):
    drug['interactions_uri'] = url_for(
        'drug_interactions', drug_type=drug['type'], drug_name=drug['name'])


def dehydrate_interaction(data):
    add_details_uri(data)
    return data


def dehydrate_interactions(data):
    for interaction in data:
        add_details_uri(interaction)
    return data


def dehydrate_drugs(data):
    for drug in data:
        add_interactions_uri(drug)
    return data
