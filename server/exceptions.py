from flask.ext.babel import gettext


class ApiError(Exception):
    pass


class RateLimitError(ApiError):
    def __init__(self, limit, per):
        message = gettext(
            'You have exceeded the maximum number of requests per minute.')
        Exception.__init__(self, message)
