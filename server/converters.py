from werkzeug.routing import BaseConverter


class DrugTypeConverter(BaseConverter):
    def __init__(self, url_map):
        super(DrugTypeConverter, self).__init__(url_map)
        self.regex = '(?:brandname|substance)'


class DrugNameConverter(BaseConverter):
    def to_python(self, value):
        value = super(DrugNameConverter, self).to_python(value)
        return value.replace('_', '/')

    def to_url(self, value):
        value = super(DrugNameConverter, self).to_url(value)
        return value.replace('/', '_')
