from time import time

from redis import Redis

from exceptions import RateLimitError


def rate_limit_check(key_prefix, limit, per):
    # Adapted from: http://flask.pocoo.org/snippets/70/
    reset = (int(time()) // per) * per + per
    key = key_prefix + str(reset)

    pipeline = Redis().pipeline()
    pipeline.incr(key)
    pipeline.expireat(key, reset + 10)

    current = pipeline.execute()[0]
    if current >= limit:
        raise RateLimitError(limit, per)
