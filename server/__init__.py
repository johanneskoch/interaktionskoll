from flask import Flask
from flask.ext.babel import Babel
from werkzeug.contrib.cache import RedisCache
from werkzeug.contrib.fixers import ProxyFix

from .converters import DrugTypeConverter, DrugNameConverter


app = Flask(__name__)
app.config.from_object('server.default_settings')
try:
    app.config.from_object('server.local_settings')
except ImportError:
    pass
app.url_map.converters.update(
    drug_type=DrugTypeConverter,
    drug_name=DrugNameConverter,
)
app.wsgi_app = ProxyFix(app.wsgi_app)
babel = Babel(app)
cache = RedisCache()

if 'SENTRY_DSN' in app.config:
    from raven.contrib.flask import Sentry
    sentry = Sentry(app)


from . import errorhandlers
from . import views
