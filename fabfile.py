import json
from os import listdir, makedirs
from os.path import dirname, exists, join

from babel.messages.pofile import read_po
from fabric.api import *


env.user = 'root'
env.hosts = ['46.246.126.228']
env.repository = '/envs/janus/janus'
env.activate = 'source /envs/janus/bin/activate'
env.server = join(dirname(env.real_fabfile), 'server')


def cleanup():
    local('find . -name "*.pyc" -delete')


def deploy():
    """ Do a git pull and restart the server """
    with cd(env.repository):
        with prefix(env.activate):
            run('git pull')
            run('pip install -r requirements/server.txt')
            run('supervisorctl restart gunicorn')


def makemessages(new_locale=None):
    with lcd(env.server):
        local('pybabel extract -F babel.cfg -o messages.pot .')
        local('pybabel update -i messages.pot -d translations')
        if new_locale:
            local('pybabel init -i messages.pot -d translations -l {}'.format(
                new_locale))


def compilemessages():
    with lcd(env.server):
        local('pybabel compile -d translations')

    # Export Javascript translation catalogs
    po_dir = join(env.server, 'translations')
    js_dir = join(env.server, 'static', 'translations')

    locales = listdir(po_dir)
    for locale in locales:
        po_path = join(po_dir, locale, 'LC_MESSAGES', 'messages.po')
        js_path = join(js_dir, '{}.js'.format(locale))

        with open(po_path) as po_file:
            catalog = read_po(po_file)

        if not exists(js_dir):
            makedirs(js_dir)

        with open(js_path, 'w') as js_file:
            catalog_dict = {msg.id: msg.string for msg in catalog if msg.id}
            js_file.writelines([
                'var catalog = {}\n'.format(json.dumps(catalog_dict)),
                'function gettext(msgid) { return catalog[msgid] || msgid; }',
            ])
